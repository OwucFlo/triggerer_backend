use std::fmt::Debug;

#[derive(Clone, PartialEq, Debug)]
pub struct Message<T: Copy + PartialEq> {
    pub chat: T,
    pub author: T,
    pub content: String
}

#[derive(Clone, PartialEq, Debug)]
pub enum Event<T: Copy + PartialEq> {
    Message(Message<T>)
}

pub trait Backend {
    type Id: Copy + PartialEq + Debug;

    fn next_event(&mut self) -> Option<Event<Self::Id>>;
    fn send_message(&mut self, chat: Self::Id, content: &str);
    fn user_name(&self, id: Self::Id) -> Option<String>;
    fn chat_name(&self, id: Self::Id) -> Option<String>;
    fn users(&self, chat: Self::Id) -> Vec<Self::Id>;
    fn chats(&self) -> Vec<Self::Id>;
}
